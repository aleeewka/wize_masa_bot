const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot('496509406:AAFmECLDXwEd_YHAzSdBaArGM8FAnWohbS8', {polling: true});
//const bot = new TelegramBot('393892806:AAGFsi7kMxsOnnfjHR9lLmam7f7p7_WMqcI', {polling: true});
const LocalStorage = require('node-localstorage').LocalStorage;



class Wash {
    constructor(){
        this.localStorage = new LocalStorage('./scratch');
        this.list = JSON.parse(this.localStorage.getItem('wash_list'));
        if(this.list===null) this.list = new Array;
        return true;
    }

    add(msgfrom) {
        for(var i=0; i < this.list.length; i++)
            if (this.list[i].id == msgfrom.id) return false;

        this.list.push(msgfrom);
        this.updateStorage();
        return true;
    }

    end(userId) {
        var res = false;
        for(var i=0; i < this.list.length; i++){
            if (this.list[i].id == userId){
                this.list.splice(i,1);
                res = true;
            }
        }
        this.updateStorage();
        return res;
    }

    endAll(){
        this.list = [];
        this.updateStorage();
    }
    updateStorage(){
        this.localStorage.setItem("wash_list",JSON.stringify(this.list));
    }
}


wash = new Wash();


var commandNotFoundMsg = new Array;
commandNotFoundMsg[0] = "Дурачек, эта команда у тебя в душе, а у меня ее нет.";
commandNotFoundMsg[1] = "Команда не найдена.";
commandNotFoundMsg[2] = "Мяу мяу. Вот и я не понимаю о чем ты.";

var options = {
    reply_markup: JSON.stringify({
        keyboard: [
            ['/wash list'],
            ['/wash add'],
            ['/wash end']
        ]
    })
};

bot.onText(/\/start/, (msg, match) => {
    const action = match[1];



    bot.sendMessage(msg.chat.id, "Всем привет. Этот бот поможет вам постирать свои грязные вещички.\n" +
    "Основные команды:\n" +
    "1. Для занятия очереди /wash add\n" +
    "2. Чтобы выйти из очереди /wash end\n" +
    "3. Для просмотра списка очереди напишите /wash list\n" +
    "4. Когда настанет ваша очередь, вам придет сообщение что пора стирать\n" +
    "5. Когда вы достираете свои вещи, нужно написать /wash end \n" +
    "6. /start для вывода подсказки", options);

});



bot.onText(/\/wash (.+)/, (msg, match) => {
    const action = match[1];

    switch(action) {
        case "add":
            if(wash.add(msg.from)){
                if(wash.list.length==1){
                    bot.sendMessage(msg.chat.id, "Скорее беги стирать, сейчас никого не должно быть. Когда закончите стирку, напиши мне /wash end", options);
                }else{
                    bot.sendMessage(msg.chat.id, "Добавлено, теперь вы "+wash.list.length+" по счету в очереди на стирку. Когда закончите стирку, напиши мне /wash end", options);
                }
            }else{
                bot.sendMessage(msg.chat.id, "Ты уже в очереди на стирку. Я напишу тебе, когда стирка будет свободна.", options);
            };
            break;

        case "list":
                if(wash.list.length>0) {
                    var _list = "";
                    for (var i = 0; i < wash.list.length; i++) _list += i + 1 + ". " + wash.list[i].first_name + " " + wash.list[i].last_name + "\n";
                    bot.sendMessage(msg.chat.id, "Очередь на стирку: Для занятия очереди напиши /wash add\n\n" + _list, options);
                }else{
                    bot.sendMessage(msg.chat.id, "В очереди никого нет. Скорее беги стирать, но не забудь /wash add", options);
                }
            break;

        case "end":
            if(wash.end(msg.from.id)){
                if(wash.list.length>0){
                    bot.sendMessage(msg.chat.id, "Ура ура ура! Очередь двинулась. Я напишу след. человеку что пора стирать.\nСлед. пользователь: "+wash.list[0].first_name, options);
                    bot.sendMessage(wash.list[0].id, "Привет. Стирка освободилась, скорее беги стирать. Когда закончишь, напиши мне /wash end чтобы я позвал следующего.", options);
                }else{
                    bot.sendMessage(msg.chat.id, "Ура ура ура! Больше в очереди никого нет.", options);
                }
            }else{
                bot.sendMessage(msg.chat.id, "Дурачек, тебя нет в очереди.", options);
            }
            break;

        case "force-end":
            if(msg.from.id=='10155071') {
                wash.endAll();
                bot.sendMessage(msg.chat.id, "Очередь чистая", options);
            }else{
                bot.sendMessage(msg.chat.id, "Дурачек, у тебя нет доступа.", options);
            }
            break;

        default:
            bot.sendMessage(msg.chat.id, commandNotFoundMsg[Math.floor(Math.random() * commandNotFoundMsg.length)]);
            break;
    };

});